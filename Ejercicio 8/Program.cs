﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_8
{
    class Program
    {
        static void Main(string[] args)
        {
            //Crear un programa que pida al usuario dos números enteros y diga &quot;Uno de los
            //números es positivo & quot;, &quot; Los dos números son positivos & quot; o bien &quot; Ninguno de los
            //números es positivo&quot;, según corresponda.

            Console.WriteLine("Inserte dos números enteros: ");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            if (a>0 && b>0)
            {
                Console.WriteLine("Los dos números son positivos");
            }
            else
            {
                if (a>0 || b>0)
                {
                    Console.WriteLine("Uno de los números es positivo");
                }
                else
                {
                    Console.WriteLine("Ninguno de los números es positivo");
                }
            }

            Console.ReadKey();
        }
    }
}
