﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Crear un programa que pida al usuario dos números enteros. Si el segundo no
            //es cero, mostrará el resultado de dividir entre el primero y el segundo. Por el
            //contrario, si el segundo número es cero, escribirá "Error: No se puede dividir
            //entre cero".

            double b = 0;
            double a = 0;
            while (b == 0)
            {
                Console.WriteLine("Inserte dos números enteros: ");
                a = Convert.ToInt32(Console.ReadLine());
                b = Convert.ToInt32(Console.ReadLine());
                if (b==0)
                {
                    Console.WriteLine("Error: No se puede dividir entre cero");
                }
            }
            double c = a / b;
            Console.WriteLine("{0}/{1}={2}",a, b, c);
            Console.ReadKey();
        }
    }
}
