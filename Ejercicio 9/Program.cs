﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_9
{
    class Program
    {
        static void Main(string[] args)
        {
            //Crear un programa que pida al usuario tres números reales y muestre cuál es el
            //mayor de los tres.

            Console.WriteLine("Inserte 3 números enteros: ");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            int c = Convert.ToInt32(Console.ReadLine());
            int[] Num = { a, b, c };
            Console.WriteLine(Num.Max() + " es el mayor de los tres.");

            Console.ReadKey();
        }
    }
}
