﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Crear un programa que pida al usuario un número entero. Si es múltiplo de 10,
            //se lo avisará al usuario y pedirá un segundo número, para decir a continuación
            //si este segundo número también es múltiplo de 10

            Console.WriteLine("Inserte un número entero: ");
            int a = Convert.ToInt32(Console.ReadLine());
            int ar = a % 10;
            if (ar==0)
            {
                Console.WriteLine("Felicidades, su número es múltiplo de 10 ");
                Console.WriteLine("Ahora inserte otro número entero: ");
                int b = Convert.ToInt32(Console.ReadLine());
                int br = b % 10;
                if (br == 0)
                {
                    Console.WriteLine("Felicidades nuevamente, resulta que este número también es múltiplo de 10");
                    Console.WriteLine("Parece que me he topado con todo un prodigio de las matemáticas");
                }
                else
                {
                    Console.WriteLine("Auch, estuviste así de cerca de ganarte el premio final");
                    Console.WriteLine("Lamento darte esta noticia pero {0} no es un múltiplo de 10", b);
                }
            }
            else
            {
                Console.WriteLine("Que lástima, este número no es múltiplo de 10");
            }
            Console.ReadKey();
        }
    }
}
