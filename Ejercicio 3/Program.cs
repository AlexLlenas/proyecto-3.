﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Crear un programa que pida al usuario dos números enteros y diga si el primero
            //es múltiplo del segundo
            Console.WriteLine("Inserte dos números enteros: ");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            double c = (a % b);
            if (c == 0)
            {
                Console.WriteLine(a + " es múltiplo de " + b);
            }
            else
            {
                Console.WriteLine(a + " no es múltiplo de " + b);
            }
            Console.ReadKey();
        }
    }
}
