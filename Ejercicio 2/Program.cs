﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Crear un programa que pida al usuario dos números enteros y diga cuál es el mayor de ellos

            Console.WriteLine("Inserte dos números enteros: ");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            if (a>b)
            {
                Console.WriteLine(a + (" en mayor que " + b));
            }
            else
            {
                Console.WriteLine(b + " en mayor que " + a);
            }
            Console.ReadKey();
        }
    }
}
