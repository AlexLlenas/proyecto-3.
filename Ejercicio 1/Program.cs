﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Crear un programa que pida al usuario un número entero y diga si es par.

            Console.WriteLine("Inserte un número entero: ");
            int n = Convert.ToInt32(Console.ReadLine());
            int r = n % 2;
            if (r == 0)
            {
                Console.WriteLine("El número es par.");
            }
            else
            {
                Console.WriteLine("El número es impar.");
            }
            Console.ReadKey();
        }
    }
}
