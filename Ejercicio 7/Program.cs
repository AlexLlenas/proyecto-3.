﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Crear un programa que pida al usuario un número enteros y diga si es múltiplo
            //de 4 o de 5.

            Console.WriteLine("Inserte un número entero: ");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = a % 4;
            int c = a % 5;
            if (b==0)
            {
                Console.WriteLine("Este número es múltiplo de 4");
            }
            else
            {
                if (c==0)
                {
                    Console.WriteLine("Este número es múltiplo de 5");
                }
                else
                {
                    Console.WriteLine("Este número no es múltiplo ni de 4 ni de 5");
                }
            }
            Console.ReadKey();
        }
    }
}
