﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5._5
{
    class Program
    {
        static void Main(string[] args)
        {
            //Crear un programa que multiplique dos números enteros de la siguiente forma:
            //pedirá al usuario un primer número entero.Si el número que se que teclee es 0,
            //escribirá en pantalla "El producto de 0 por cualquier número es 0". Si se ha
            //tecleado un número distinto de cero, se pedirá al usuario un segundo número y
            //se mostrará el producto de ambos.

            var a = 0;
            while (a == 0)
            {
                Console.WriteLine("Favor intertar un número entero: ");
                a = Convert.ToInt32(Console.ReadLine());
                if (a == 0)
                {
                    Console.WriteLine("El producto de 0 por cualquier número es 0");
                }
            }
            var b = 0;
            while (b==0)
            {
                Console.WriteLine("Inserte un segundo número entero: ");
                b = Convert.ToInt32(Console.ReadLine());
                if (b == 0)
                {
                    Console.WriteLine("El producto de 0 por cualquier número es 0");
                }
            }
            int c = a * b;
            Console.WriteLine("{0} x {1} = {2}", a, b, c);
            Console.ReadKey();
        }
    }
}
